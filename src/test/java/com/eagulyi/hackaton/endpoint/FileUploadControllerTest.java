package com.eagulyi.hackaton.endpoint;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.*;

/**
 * Created by eugene on 3/4/17.
 */
@SpringBootTest
public class FileUploadControllerTest {
    private String incomingFile = "src/test/resources/1svid.mp4";
    private String savedFile = "/Users/eugene/IdeaProjects/bitbucket-repo/lviv-mobile/upload-dir/1svid.mp4";

    @Test
    public void testUpload() throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost post = new HttpPost("http://localhost:8080/upload");
        Files.createFile(Paths.get(savedFile));
        InputStream inputStream = new FileInputStream(new File(savedFile));
        File file = new File(incomingFile);
        String message = "This is a multipart video post";
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.addBinaryBody
                ("upfile", file, ContentType.DEFAULT_BINARY, incomingFile);
        builder.addBinaryBody
                ("upstream", inputStream, ContentType.create("application/zip"), savedFile);
        builder.addTextBody("file", message, ContentType.MULTIPART_FORM_DATA);
        HttpEntity entity = builder.build();
        post.setEntity(entity);
        HttpResponse response = client.execute(post);
        Assert.assertTrue(Files.exists(Paths.get(savedFile)));
    }

    @After
    public void cleanup() throws IOException {
        if (Files.exists(Paths.get(savedFile)))
            Files.delete(Paths.get(savedFile));
    }


}