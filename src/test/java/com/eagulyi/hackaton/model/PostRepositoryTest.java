package com.eagulyi.hackaton.model;

import com.eagulyi.hackaton.data.PostRepository;
import com.eagulyi.hackaton.data.ReactionRepository;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by dmytryguly on 3/4/17.
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class PostRepositoryTest {
   @Autowired
   PostRepository postRepository;
   @Autowired
   ReactionRepository reactionRepository;

   @Test
   public void testSavePosts() {
      Post post = new Post("bla", LocalDateTime.now());
      postRepository.save(post);
      Assert.assertTrue(postRepository.count() == 1);
   }

   @Test
   public void testSaveReactions() {
      Post post = new Post("bla", LocalDateTime.now());
      postRepository.save(post);

      Reaction reaction = new Reaction(post);
      reactionRepository.save(reaction);
      List<Reaction> reactions = reactionRepository.findAll();
      Assert.assertTrue(reactions.size() == 1);

      List<Post> posts = postRepository.findAll();
      Assert.assertTrue(posts.get(0).getReactions() != null);
   }

   @Test
   public void testPostReactionRelation() {
      Post post = new Post("bla", LocalDateTime.now());
      post.setUuid("post1-uuid");
      postRepository.save(post);
      List<Post> posts = postRepository.findAll();
      Assert.assertTrue(posts.size() == 1);

      Post savedPost = posts.get(0);
      Reaction reaction = new Reaction(savedPost);
      post.setReactions(Lists.newArrayList(reaction));

      postRepository.save(post);
      List<Reaction> reactions = reactionRepository.findAll();
      assert reactions.get(0).getPost() != null;
   }

//   @Test
//   public void testFindByUuid(){
//      Post post = new Post("bla");
//      post.setUuid("cfee086a-f8d7-4c7b-813d-f1b53b0e56f6");
//      postRepository.save(post);
//      Assert.assertNotNull(postRepository.getByUuid("cfee086a-f8d7-4c7b-813d-f1b53b0e56f6"));
//   }

}