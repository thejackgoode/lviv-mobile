package com.eagulyi.hackaton.storage;

import com.eagulyi.hackaton.data.PostRepository;
import com.eagulyi.hackaton.model.Post;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by eugene on 3/4/17.
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class FileSystemStorageServiceTest {
    private String incomingFile = "src/test/resources/1svid.mp4";

    @Autowired
    StorageService storageService;

    @Autowired
    PostRepository postRepository;

    @Test
    public void storePost() throws IOException {
        MockMultipartFile firstFile = new MockMultipartFile("file", "1svid.mp4", "multipart/form-data", Files.readAllBytes(Paths.get(incomingFile)));
        Post post = new Post("test", firstFile);
        postRepository.save(post);
        storageService.store(post);
        postRepository.findAll();
    }

}