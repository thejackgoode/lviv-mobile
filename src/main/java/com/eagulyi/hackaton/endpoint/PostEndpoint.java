package com.eagulyi.hackaton.endpoint;

import com.eagulyi.hackaton.data.PostRepository;
import com.eagulyi.hackaton.model.Post;
import com.eagulyi.hackaton.model.Reaction;
import com.eagulyi.hackaton.storage.StorageFileNotFoundException;
import com.eagulyi.hackaton.storage.StorageService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by eugene on 3/4/17.
 */
@RestController
@RequestMapping(value = "/posts")
public class PostEndpoint {
    private final StorageService storageService;
    private final PostRepository postRepository;

    @Value("${com.reakt.video.rootLocation}")
    private String location;

    @Autowired
    public PostEndpoint(StorageService storageService, PostRepository postRepository) {
        this.storageService = storageService;
        this.postRepository = postRepository;
    }

    @GetMapping(produces = "application/json")
    public List<Post> getPosts() {
        return Collections.emptyList();
    }

    @RequestMapping(value = "/postStream", produces = "multipart/form-data", method = RequestMethod.GET)
    public void getPostVideoStream(@RequestParam(name = "uuid") String uuid, HttpServletResponse response) throws IOException {
        Path path = Paths.get(location).resolve(uuid);
        Path file = Files.list(path).filter(e -> e.toFile().getName().endsWith(".mp4") || e.toFile().getName().endsWith(".m4v")).findAny().get();

        IOUtils.copy(Files.newInputStream(file), response.getOutputStream());
//        response.setContentType("video/mp4");
        response.flushBuffer();
    }

    @RequestMapping(value = "/postPreview", produces = MediaType.IMAGE_PNG_VALUE, method = RequestMethod.GET)
    public void getPostPreview(@RequestParam(name = "uuid") String uuid, HttpServletResponse response) throws IOException {
        Path path = Paths.get(location).resolve(uuid);
        Path file = Files.list(path).filter(e -> e.toFile().getName().endsWith(".png")).findAny().get();

        IOUtils.copy(Files.newInputStream(file), response.getOutputStream());
        response.flushBuffer();
    }

    @RequestMapping(value = "/postDescription", produces = "text/plain", method = RequestMethod.GET)
    public String getPostDescription(@RequestParam(name = "uuid") String uuid, HttpServletResponse response) throws IOException {
        Path path = Paths.get(location).resolve(uuid);
        Path file = Files.list(path).filter(e -> e.toFile().getName().endsWith(".txt")).findAny().get();

        return String.valueOf(Files.readAllLines(file));
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<InputStreamResource> getAcquisition() throws FileNotFoundException{
        InputStreamResource inputStreamResource = new InputStreamResource(new FileInputStream("/Users/eugene/IdeaProjects/bitbucket-repo/lviv-mobile/upload-dir/9ff74ce6-f80f-4a4c-8e1a-f1cd62ca772e/VID_20170305_133205.mp4"));
        return new ResponseEntity(inputStreamResource, HttpStatus.OK);
    }

    @RequestMapping(value = "/feed", produces = "application/json", method = RequestMethod.GET)
    public List<Post> getFeed(HttpServletResponse response) throws IOException {
        List<Post> posts = new ArrayList<>();
        Files.list(Paths.get(location)).forEach(e -> {
            Post post = new Post();
            String uuid = e.toFile().getName();
            if (!uuid.equals(".DS_Store")) {
                post.setUuid(uuid);
                post.setVideoLink("http://10.10.11.234:8080/posts/postStream?uuid=" + uuid);
                post.setPreviewLink("http://10.10.11.234:8080/posts/postPreview?uuid=" + uuid);

                Path path = Paths.get(location).resolve(uuid);
                try {
                    Path file = Files.list(path).filter(r -> r.toFile().getName().endsWith(".txt")).findAny().get();

                    BufferedReader reader = new BufferedReader(new FileReader(file.toFile()));
                    String[] fields = reader.readLine().split(":::");
                    post.setDescription(fields[0]);
                    if (fields.length > 1) post.setDateTime(LocalDateTime.parse(fields[1]));

                    List<Reaction> reactions = new ArrayList<>();
                    Files.list(path)
                            .forEach(reaction -> {
                                if (Files.isDirectory(reaction)) {
                                    Reaction newReaction = new Reaction();
                                    newReaction.setUuid(reaction.toFile().getName());
                                    newReaction.setVideoLink("http://10.10.11.234:8080/reactions/reactionStream?postUuid=" + uuid +
                                            "&reactionUuid=" + reaction.toFile().getName());
                                    newReaction.setPreviewLink("http://10.10.11.234:8080/reactions/reactionPreview?postUuid=" + uuid +
                                            "&reactionUuid=" + reaction.toFile().getName());
                                    newReaction.setPost(post);
                                    reactions.add(newReaction);
                                }
                            });
                    post.setReactions(reactions);
                    posts.add(post);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        posts.sort(Comparator.comparing(Post::getDateTime).reversed());
        return posts;
    }

    @PostMapping("/upload")
    public void handleFileUpload(@RequestParam("description") String description,
                                 @RequestParam("file") MultipartFile file) {
        Post post = new Post(description, file);
        storageService.store(post);

        //post = postRepository.save(post);
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

}