package com.eagulyi.hackaton.endpoint;

import com.eagulyi.hackaton.model.Reaction;
import com.eagulyi.hackaton.storage.StorageService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by eugene on 3/4/17.
 */
@RestController
@RequestMapping(value = "/reactions")
public class ReactionEndpoint {

    private final StorageService storageService;

    @Value("${com.reakt.video.rootLocation}")
    private String location;

    @Autowired
    public ReactionEndpoint(StorageService storageService) {
        this.storageService = storageService;
    }


    @PostMapping("/upload")
    public void handleFileUpload(@RequestParam("uuid") String postUuid,
                                 @RequestParam("file") MultipartFile file) {
        Reaction reaction = new Reaction(file);

        storageService.store(reaction, postUuid);
    }

    @RequestMapping(value = "/reactionStream", produces = "multipart/form-data", method = RequestMethod.GET)
    public void getReactionVideoStream(@RequestParam(name = "postUuid") String postUuid,
                                       @RequestParam(name = "reactionUuid") String reactionUuid,
                                       HttpServletResponse response) throws IOException {
        Path path = Paths.get(location).resolve(postUuid).resolve(reactionUuid);
        Path videoFile = Files.list(path).filter(e -> e.toFile().getName().endsWith(".mp4")|| e.toFile().getName().endsWith(".m4v")).findAny().get();

        IOUtils.copy(Files.newInputStream(videoFile), response.getOutputStream());
        response.flushBuffer();
    }

    @RequestMapping(value = "/reactionPreview", produces = MediaType.IMAGE_PNG_VALUE, method = RequestMethod.GET)
    public void getPostPreview(@RequestParam(name = "postUuid") String postUuid,
                               @RequestParam(name = "reactionUuid") String reactionUuid,
                               HttpServletResponse response) throws IOException {
        Path path = Paths.get(location).resolve(postUuid).resolve(reactionUuid);
        Path file = Files.list(path).filter(e -> e.toFile().getName().endsWith(".png")).findAny().get();

        IOUtils.copy(Files.newInputStream(file), response.getOutputStream());
        response.flushBuffer();

    }

}
