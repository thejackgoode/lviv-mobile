package com.eagulyi.hackaton.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by eugene on 3/4/17.
 */
@Entity
@Table(name = "reaction")
public class Reaction extends Video {

   private Post post;

   public Reaction() {}

   public Reaction(Post post) {
      this.post = post;
   }

   public Reaction(Post post, MultipartFile file) {
      this.post = post;
      setVideoFile(file);
   }

   public Reaction(MultipartFile file) {
      setVideoFile(file);
   }

   @JsonIgnore
   @ManyToOne(fetch = FetchType.EAGER)
   @JoinColumn(name = "post_id")
   public Post getPost() {
      return post;
   }

   public void setPost(Post post) {
      this.post = post;
   }
}
