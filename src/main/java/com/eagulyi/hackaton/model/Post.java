package com.eagulyi.hackaton.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by eugene on 3/4/17.
 */
@Entity
@Table(name = "post")
public class Post extends Video {
    private List<Reaction> reactions;

    private String description;
    private LocalDateTime dateTime;

    public Post() {
    }

    public Post(String uuid, String description){
        setUuid(uuid);
    }

    public Post(String description, LocalDateTime dateTime) {
        this.description = description;
        this.dateTime = dateTime;
    }

    public Post(String description, MultipartFile videoFile) {
        this.description = description;
        setVideoFile(videoFile);
    }

    public Post(MultipartFile videoFile) {
        setVideoFile(videoFile);
    }

    @OneToMany(mappedBy = "post", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    public List<Reaction> getReactions() {
        return reactions;
    }

    public void setReactions(List<Reaction> reactions) {
        this.reactions = reactions;
    }

    @Column(nullable = false)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        return "Post{" +
              "reactions=" + reactions +
              ", description='" + description + '\'' +
              '}';
    }


}
