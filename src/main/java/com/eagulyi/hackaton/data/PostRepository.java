package com.eagulyi.hackaton.data;

import com.eagulyi.hackaton.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by eugene on 3/4/17.
 */
public interface PostRepository extends JpaRepository<Post, String> {
    public Post getByUuid(String Uuid);
}
