package com.eagulyi.hackaton.data;

import com.eagulyi.hackaton.model.Post;
import com.eagulyi.hackaton.model.Reaction;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by dmytryguly on 3/4/17.
 */
public interface ReactionRepository extends JpaRepository<Reaction, String> {
    Reaction getByUuid(String Uuid);
}
