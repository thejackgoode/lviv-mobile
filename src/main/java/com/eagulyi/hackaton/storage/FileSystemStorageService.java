package com.eagulyi.hackaton.storage;

import com.eagulyi.hackaton.model.Post;
import com.eagulyi.hackaton.model.Reaction;
import com.eagulyi.hackaton.utils.PreviewCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

@Service
public class FileSystemStorageService implements StorageService {

    private Path rootLocation;

    @Autowired
    public FileSystemStorageService(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
    }

    @Override
    public void store(MultipartFile file) {
        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + file.getOriginalFilename());
            }
            Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()));
        } catch (IOException e) {
            throw new StorageException("Failed to store file " + file.getOriginalFilename(), e);
        }
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.rootLocation, 1)
                    .filter(path -> !path.equals(this.rootLocation))
                    .map(path -> this.rootLocation.relativize(path));
        } catch (IOException e) {
            throw new StorageException("Failed to read stored files", e);
        }

    }

    @Override
    public Path load(String filename) {
        return rootLocation.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if(resource.exists() || resource.isReadable()) {
                return resource;
            }
            else {
                throw new StorageFileNotFoundException("Could not read file: " + filename);
            }
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    @Override
    public void store(Post post) {
        System.out.println("Started storing post ...");
        MultipartFile videoFile = post.getVideoFile();
        try {
            if (videoFile.isEmpty()) {
                throw new StorageException("Failed to store empty file " + videoFile.getOriginalFilename());
            }
            Path folder = this.rootLocation.resolve(Paths.get(post.getUuid()));
            Files.createDirectories(folder);
            Path fullPath = folder.resolve(videoFile.getOriginalFilename());
            Files.copy(videoFile.getInputStream(), fullPath);
            PreviewCreator.create(fullPath.toString());
            DescriptionCreator.create(fullPath, post.getDescription());
        } catch (IOException e) {
            throw new StorageException("Failed to store file " + videoFile.getOriginalFilename(), e);
        }
        System.out.println("Finished storing post!");
    }

    @Override
    public void store(Reaction reaction, String postUuid) {
        System.out.println("Started storing reaction ... ");
        MultipartFile videoFile = reaction.getVideoFile();
        try {
            if (videoFile.isEmpty()) {
                throw new StorageException("Failed to store empty file " + videoFile.getOriginalFilename());
            }
            Path folder = this.rootLocation.resolve(Paths.get(postUuid)).resolve(reaction.getUuid());
            Files.createDirectories(folder);
            Path fullPath = folder.resolve(videoFile.getOriginalFilename());
            Files.copy(videoFile.getInputStream(), fullPath);
            PreviewCreator.create(fullPath.toString());
        } catch (IOException e) {
            throw new StorageException("Failed to store file " + videoFile.getOriginalFilename(), e);
        }
        System.out.println("Finished storing reaction!");
    }

    @Override
    public void init() {
        try {
            Files.createDirectory(rootLocation);

        } catch (IOException e) {
            throw new StorageException("Could not initialize storage", e);
        }
    }
}
