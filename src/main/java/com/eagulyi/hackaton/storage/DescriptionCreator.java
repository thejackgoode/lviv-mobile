package com.eagulyi.hackaton.storage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;

/**
 * Created by eugene on 3/5/17.
 */
public class DescriptionCreator {
    public static void create(Path path, String description) throws IOException {
        Path descriptionFilePath = Files.createFile(Paths.get(path + ".txt"));
        Files.write(descriptionFilePath, description.getBytes());
        Files.write(descriptionFilePath, (":::" + LocalDateTime.now().toString()).getBytes(), StandardOpenOption.APPEND);

    }
}
